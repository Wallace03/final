/* global Backendless, Handlebars, moment */

$(function () {
    var APPLICATION_ID = "EE00E6F0-F01B-FFBD-FFB8-3176718D6800",
        SECRET_KEY = "EEB5F351-4DF8-1022-FF5D-8C7D575BC200",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log("postsCollection");
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment (time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHtml = blogTemplate(wrapper);
    
    $('.main-container').html(blogHtml);
$(document).on('click', '.white-out-post', function(){
               var checkListScript = $("#check-done-template").html();
               var checkListTemplate = Handlebars.compile(checkListScript);
               
               
                   
               $('.main-container').html(checkListTemplate);
               Materialize.toast('task has been checked', 4000);
            });
            
           
            $(document).on('click', '.white-out-post', function(){
               var uncheckListScript = $("#check-done-template").html();
               var uncheckListTemplate = Handlebars.compile(uncheckListScript);
               $('.main-container').html(uncheckListTemplate);
               Materialize.toast('task has been unchecked', 4000);
           });
});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

$(document).on('click', '.trash', function(event){
    console.log(event);
    Backendless.Persistence.of(Posts).remove(event.target.attributes.data.nodeValue);
    location.reload();
    Materialize.toast('task has been deleted', 4000);
});
